package ejercicio.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ejercicio.model.Ciudad;
import ejercicio.model.Contacto;

public interface ContactoRepo extends JpaRepository<Contacto , Integer> {

	@Query("Select c from Contacto c where lower(c.nombre) like %:nombre%")
	List<Contacto> findByNombreContainingIgnoreCase(@Param("nombre")String nombre);
	
	List<Contacto> findByCiudad(Ciudad ciudad);
	
	@Query("Select c from Contacto c join c.ciudad ciudad where ciudad.descripcion like %:ciudad%")
	List<Contacto> findByNombreCiudadContainingIgnoreCase(@Param("ciudad") String ciudad);
	
	@Query("Select c from Contacto c join c.ciudad ciudad where c.nombre like %:nombre% or ciudad.descripcion like %:ciudad%")
	List<Contacto> findByNombreAndCiudadContainingIgnoreCase(@Param("nombre")String nombre, @Param("ciudad") String ciudad);
}
