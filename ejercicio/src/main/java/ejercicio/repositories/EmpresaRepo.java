package ejercicio.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ejercicio.model.Empresa;

public interface EmpresaRepo extends JpaRepository<Empresa, Integer>{

}
