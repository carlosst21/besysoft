package ejercicio.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ejercicio.model.Ciudad;

public interface CiudadRepo  extends JpaRepository<Ciudad , Integer>{

}
