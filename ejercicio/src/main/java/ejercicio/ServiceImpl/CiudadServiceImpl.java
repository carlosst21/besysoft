package ejercicio.ServiceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import ejercicio.model.Ciudad;
import ejercicio.repositories.CiudadRepo;
import ejercicio.service.CiudadService;

@Service
public class CiudadServiceImpl implements CiudadService{

	@Autowired
	private CiudadRepo repo; 
	
	@Override
	public Ciudad registrar(Ciudad obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Ciudad modificar(Ciudad obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Ciudad> listar() {
		System.out.println("entra listar");
		return repo.findAll();
	}

	@Override
	public Ciudad listarPorId(Integer id) {
		Optional<Ciudad> op = repo.findById(id);
		return op.isPresent() ? op.get() : new Ciudad();
	}

	@Override
	public boolean eliminar(Integer id) {
		// TODO Auto-generated method stub
		return false;
	}

}
