package ejercicio.ServiceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import ejercicio.model.Empresa;
import ejercicio.repositories.EmpresaRepo;
import ejercicio.service.EmpresaService;

@Service
public class EmpresaServiceImpl implements EmpresaService {

	@Autowired
	private EmpresaRepo repo;

	@Override
	public Empresa registrar(Empresa obj) {
		return repo.save(obj);
		
	}

	@Override
	public Empresa modificar(Empresa obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Empresa> listar() {
		return repo.findAll();
	}

	@Override
	public Empresa listarPorId(Integer id) {
		Optional<Empresa> op = repo.findById(id);
		return op.isPresent() ? op.get() : new Empresa();
	}

	@Override
	public boolean eliminar(Integer id) {
		// TODO Auto-generated method stub
		return false;
	}
}
