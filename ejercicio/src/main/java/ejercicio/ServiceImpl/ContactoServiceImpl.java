package ejercicio.ServiceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ejercicio.model.Ciudad;
import ejercicio.model.Contacto;
import ejercicio.repositories.ContactoRepo;
import ejercicio.service.ContactoService;

@Service
public class ContactoServiceImpl implements ContactoService {

	@Autowired
	private ContactoRepo repo;

	@Override
	public Contacto registrar(Contacto obj) {
		return repo.save(obj);
	}

	@Override
	public Contacto modificar(Contacto obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Contacto> listar() {
		return repo.findAll();
	}

	@Override
	public Contacto listarPorId(Integer id) {
		Optional<Contacto> op = repo.findById(id);
		return op.isPresent() ? op.get() : new Contacto();
	}

	@Override
	public boolean eliminar(Integer id) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public List<Contacto> buscarPorNombre(String nombre){
		return repo.findByNombreContainingIgnoreCase(nombre);
	}
	
	public List<Contacto> buscarPorCiudad(Ciudad ciudad){
		return repo.findByCiudad(ciudad);
	}

	@Override
	public List<Contacto> buscarPorCiudadNombre(String ciudad) {
		return repo.findByNombreCiudadContainingIgnoreCase(ciudad);
	}

	@Override
	public List<Contacto> buscarPorNombreAndCiudad(String nombre, String ciudad) {
		return repo.findByNombreAndCiudadContainingIgnoreCase(nombre, ciudad);
	}
	
	
}
