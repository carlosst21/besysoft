package ejercicio.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="empresas")
public class Empresa {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "razonsocial", nullable = false, length = 100)
	private String razonsocial;
	@Column(name = "direccion", nullable = false, length = 150)
	private String direccion;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "empresa", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private List<Contacto> contactos;
	
	public Empresa() {
		super();
	}

	public Empresa(String razonsocial, String direccion) {
		super();
		this.razonsocial = razonsocial;
		this.direccion = direccion;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRazonsocial() {
		return razonsocial;
	}

	public void setRazonsocial(String razonsocial) {
		this.razonsocial = razonsocial;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public List<Contacto> getContactos() {
		return contactos;
	}

	public void setContactos(List<Contacto> contactos) {
		this.contactos = contactos;
	}

	public Integer countContactos() {
		return this.contactos.size();
	}
	
}
