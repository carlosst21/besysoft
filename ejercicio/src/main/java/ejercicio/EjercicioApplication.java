package ejercicio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import ejercicio.controller.CiudadController;
import ejercicio.controller.ContactoController;
import ejercicio.controller.EmpresaController;
import ejercicio.model.Ciudad;
import ejercicio.model.Contacto;
import ejercicio.model.Empresa;
import javassist.NotFoundException;

@SpringBootApplication
public class EjercicioApplication implements CommandLineRunner {

	@Autowired
	CiudadController ciu;
	@Autowired
	EmpresaController emp;
	@Autowired
	ContactoController cont;

	public static void main(String[] args) {
		SpringApplication.run(EjercicioApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		//Integer intEmpresa = 0;
		int ciclo = 0;

		try {
			BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
			String texto = "";
			char opcion = '1';
			while ((opcion == '1') || (opcion == '2') || (opcion == '3') || (opcion == '4') || (opcion == '5')
					|| (opcion == '6')) {
				System.out.println(" ");
				System.out.println("AGENDA");
				System.out.println("======");
				System.out.println("1-Nueva Empresa");
				System.out.println("2-Nuevo Contacto");
				System.out.println("3-Buscar Contacto por Nombre");
				System.out.println("4-Buscar Contacto por Ciudad");
				System.out.println("5-Buscador Complejo");
				System.out.println("6-Listado de Empresas");
				System.out.println("0-Salir");
				System.out.println(" ");
				System.out.print("Opción: ");
				texto = teclado.readLine();
				opcion = texto.charAt(0);

				switch (opcion) {
				case '1':
					nuevaEmprea();
					break;
				case '2':
					nuevocontacto();
					break;
				case '3':
					System.out.println("Buscar Contacto por Nombre:");
					System.out.println("Introduzca Nombre:");
					String nombre = "";
					nombre = teclado.readLine();
					List<Contacto> listaxnombre = cont.buscarPorNombre(nombre);
					for (ciclo = 0; ciclo < listaxnombre.size(); ciclo++) {
						System.out.println(listaxnombre.get(ciclo).getId() + " - " + listaxnombre.get(ciclo).getNombre()
								+ " (Empresa:" + listaxnombre.get(ciclo).getEmpresa().getRazonsocial() + ")");

					}
					break;
				case '4':
					System.out.println("Buscar Contacto por Ciudad:");
					List<Ciudad> lista = ciu.listar();
					for (ciclo = 0; ciclo < lista.size(); ciclo++) {
						System.out.print(lista.get(ciclo).getId() + " - " + lista.get(ciclo).getDescripcion() + " / ");
					}
					System.out.println("");
					System.out.println("Ingrese Opcion:");
					String srtCiudad = teclado.readLine();
					if (esNumero(srtCiudad)) {
						int intciudad = Integer.parseInt(srtCiudad);
						Ciudad ciudad = ciu.buscarPorId(intciudad);
						if (ciudad.getId() != null) {
							List<Contacto> listaxciudad = cont.buscarPorCiudad(ciudad);
							for (ciclo = 0; ciclo < listaxciudad.size(); ciclo++) {
								System.out.println(listaxciudad.get(ciclo).getId() + " - " + listaxciudad.get(ciclo).getNombre()
										+ " (Empresa:" + listaxciudad.get(ciclo).getEmpresa().getRazonsocial() + ")");
							}
						} else {
							System.out.println("ERROR - El Nro ingresado no corresponde a una ciudad");
						}
					} else {
						System.out.println("ERROR - Debe ingresar un nro de ciudad");
					}
					break;
				case '5':
					System.out.println("Buscador combinado:");
					System.out.println("Introduzca Nombre a buscar (deje en blanco para omitir parametro):");
					String buscanombre = "";
					buscanombre = teclado.readLine();
					System.out.println("Introduzca ciudad a buscar (deje en blanco para omitir parametro):");
					String buscaciudad = "";
					buscaciudad = teclado.readLine();
					List<Contacto> listabuscar;
					if (buscanombre.trim().isEmpty()) {
						if (buscaciudad.trim().isEmpty()) {
							System.out.println("No se cargaron parametros de busqueda, se listaran todos los contactos");
							listabuscar = cont.listar();
						} else {
							listabuscar = cont.buscarPorNombreCiudad(buscaciudad);
						}
					} else {
						if (buscaciudad.trim().isEmpty()) {
							listabuscar = cont.buscarPorNombre(buscanombre);
						} else {
							listabuscar = cont.buscarPorNombreAndCiudad(buscanombre, buscaciudad);
						}
					}
					for(ciclo=0;ciclo<listabuscar.size();ciclo++) {
						System.out.println(listabuscar.get(ciclo).getId() + " - " + listabuscar.get(ciclo).getNombre()
								+ " - Ciudad: " + listabuscar.get(ciclo).getCiudad().getDescripcion() + "  " 
								+ " (Empresa:" + listabuscar.get(ciclo).getEmpresa().getRazonsocial() + ")");
					}
					break;
				case '6':
					System.out.println("Listado de Empresas:");
					ciclo = 0;
					List<Empresa> listaempresa = emp.listar();

					for (ciclo = 0; ciclo < listaempresa.size(); ciclo++) {
						System.out.println("Empresa: " + listaempresa.get(ciclo).getRazonsocial() + "  - Direccion: "
								+ listaempresa.get(ciclo).getDireccion() + " ("
								+ listaempresa.get(ciclo).countContactos() + " contactos)");
					}
					break;
				case '0':
					System.out.println("Ha salido del programa");
					break;
				default:
					System.out.println("Opción incorrecta ...");
					opcion = '1';
				}
			}
		} catch (IOException ex) {
			System.out.println("Se produjo un error: " + ex.getMessage());
		} catch (NotFoundException ex) {
			System.out.println("Se produjo un error: " + ex.getMessage());
		}
	}

	public static boolean esNumero(String str) {
		boolean result = str.matches("[+-]?\\d*(\\.\\d+)?");
		return result;
	}

	public void nuevaEmprea() {
		BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
		try {
		System.out.println("Nueva Empresa:");
		String razonSocial = "";
		String direccionEmpresa = "";
		System.out.println("Introduzca la Razon Social:");
		razonSocial = teclado.readLine();
		System.out.println("Introduzca la Direccion:");
		direccionEmpresa = teclado.readLine();
		if (!razonSocial.trim().isEmpty()) {
			if (emp.nuevo(razonSocial, direccionEmpresa)) {
				System.out.println("Se registro la empresa " + razonSocial);
			} else {
				System.out.println("Fallo el registro de la empresa " + razonSocial);
			}
		} else {
			System.out.println("ERROR -- No ingreso Razon Social");
		}
		} catch (IOException ex) {
			System.out.println("Se produjo un error: " + ex.getMessage());
		} 
	}
	
	public void nuevocontacto() {
		Integer intEmpresa = 0;
		int ciclo = 0;
		BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
		try {
			System.out.println("Nuevo Contacto:");
			List<Empresa> empresas = emp.listar();
			if (empresas.size() > 0) {
				System.out.println("Nuevo Contacto:");
				System.out.println("Ingrese a que Empresa Agrega ese nuevo Contacto: ");
				for (ciclo = 0; ciclo < empresas.size(); ciclo++) {
					System.out.println(ciclo + 1 + " - " + empresas.get(ciclo).getRazonsocial());
				}
				System.out.println("Ingrese Opcion:");
				String nroEmpresa = teclado.readLine();
				String nombre = "";
				String telefono = "";
				String direccionContacto = "";
				Integer intciudad = 0;
				String srtCiudad = "";
				String mail = "";

				System.out.println("Introduzca Nombre:");
				nombre = teclado.readLine();
				System.out.println("Introduzca la Direccion:");
				direccionContacto = teclado.readLine();
				System.out.println("Introduzca Telefono:");
				telefono = teclado.readLine();
				System.out.println("Introduzca Ciudad:");
				List<Ciudad> lista = ciu.listar();
				for (ciclo = 0; ciclo < lista.size(); ciclo++) {
					System.out.print(
							lista.get(ciclo).getId() + " - " + lista.get(ciclo).getDescripcion() + " / ");
				}
				System.out.println("");
				System.out.println("Ingrese Opcion:");
				srtCiudad = teclado.readLine();
				System.out.println("Introduzca Mail:");
				mail = teclado.readLine();
				if (esNumero(nroEmpresa)) {
					intEmpresa = Integer.parseInt(nroEmpresa);
					Empresa empresa = emp.buscarPorId(intEmpresa);
					if (empresa.getId() != null) {
						if (!nombre.trim().isEmpty()) {
							if (esNumero(srtCiudad)) {
								intciudad = Integer.parseInt(srtCiudad);
								Ciudad ciudad = ciu.buscarPorId(intciudad);
								if (ciudad.getId() != null) {
									if (cont.nuevo(nombre, direccionContacto, telefono, mail, ciudad,
											empresa)) {
										System.out.println("Se registro el Contacto " + nombre);
									} else {
										System.out.println("Fallo el registro del Contacto " + nombre);
									}
								} else {
									System.out.println("ERROR - El Nro ingresado no corresponde a una ciudad");
								}
							} else {
								System.out.println("ERROR - Debe ingresar un nro de ciudad");
							}
						} else {
							System.out.println("ERROR - Debe ingresar un Nombre");
						}
					} else {
						System.out.println("ERROR - El Nro ingresado no corresponde a una empresa");
					}
				} else {
					System.out.println("ERROR - Debe ingresar un nro de empresa");
				}
			} else {
				System.out.println("Debe ingresar una Empresa primero");
			}
		}catch (IOException ex) {
			System.out.println("Se produjo un error: " + ex.getMessage());
		} catch (NotFoundException ex) {
			System.out.println("Se produjo un error: " + ex.getMessage());
		}
	}
	
	
	
	
}
