package ejercicio.service;

import java.util.List;

import ejercicio.model.Ciudad;
import ejercicio.model.Contacto;

public interface ContactoService extends ICRUD<Contacto, Integer> {

	public List<Contacto> buscarPorNombre(String nombre);
	public List<Contacto> buscarPorCiudad(Ciudad ciudad);
	public List<Contacto> buscarPorCiudadNombre(String ciudad);
	public List<Contacto> buscarPorNombreAndCiudad(String nombre, String ciudad);
}
