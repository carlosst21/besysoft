package ejercicio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ejercicio.model.Ciudad;
import ejercicio.model.Contacto;
import ejercicio.model.Empresa;
import ejercicio.service.ContactoService;
import javassist.NotFoundException;

@Controller
public class ContactoController {

	@Autowired
	private ContactoService service;
	
	public List<Contacto> listar() {
		List<Contacto> lista = service.listar();
		return lista;
	}
	
	public boolean nuevo(String nombre, String direccion, String telefono, String mail, Ciudad ciudad, Empresa empresa) {
		Contacto obj = new Contacto(nombre, direccion, telefono, mail, ciudad, empresa);
		Contacto obj1 = service.registrar(obj);
		if (obj1 != null) { 
			return true;
		}
		return false;
	}
	
	public Contacto buscarPorId(int id) throws NotFoundException {
		Contacto obj = service.listarPorId(id);
		if (obj.getId() == null) {
			throw new NotFoundException("Id no encontrado " + id);
		}
		return obj;
	}
	
	public List<Contacto> buscarPorNombre(String nombre){
		List<Contacto> lista = service.buscarPorNombre(nombre);
		return lista;
	}
	
	public List<Contacto> buscarPorCiudad(Ciudad ciudad){
		List<Contacto> lista = service.buscarPorCiudad(ciudad);
		return lista;
	}
	
	public List<Contacto> buscarPorNombreCiudad(String ciudad){
		List<Contacto> lista = service.buscarPorCiudadNombre(ciudad);
		return lista;
	}
	
	public List<Contacto> buscarPorNombreAndCiudad(String nombre, String ciudad){
		List<Contacto> lista = service.buscarPorNombreAndCiudad(nombre, ciudad);
		return lista;
	}
	
}
