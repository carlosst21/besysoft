package ejercicio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ejercicio.model.Ciudad;
import ejercicio.service.CiudadService;
import javassist.NotFoundException;

@Controller
public class CiudadController {
	
	@Autowired
	private CiudadService service;
	
	public List<Ciudad> listar() {
		List<Ciudad> lista = service.listar();
		return lista;
	}
	
	public Ciudad buscarPorId(int id) throws NotFoundException {
		Ciudad obj = service.listarPorId(id);
		if (obj.getId() == null) {
			throw new NotFoundException("Id no encontrado " + id);
		}
		return obj;
	}

}
