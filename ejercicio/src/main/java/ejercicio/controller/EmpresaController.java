package ejercicio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;


import ejercicio.model.Empresa;
import ejercicio.service.EmpresaService;
import javassist.NotFoundException;

@Controller
public class EmpresaController {

	@Autowired
	private EmpresaService service;
	
	public List<Empresa> listar() {
		System.out.println("Entra a listar");
		List<Empresa> lista = service.listar();
		return lista;
	}
	
	public boolean nuevo(String razonSocial, String direccionEmpresa) {
		Empresa obj = new Empresa(razonSocial, direccionEmpresa);
		Empresa obj1 = service.registrar(obj);
		if (obj1 != null) { 
			return true;
		}
		return false;
	}
	
	public Empresa buscarPorId(int id) throws NotFoundException {
		Empresa obj = service.listarPorId(id);
		if (obj.getId() == null) {
			throw new NotFoundException("Id no encontrado " + id);
		}
		return obj;
	}
	
}
