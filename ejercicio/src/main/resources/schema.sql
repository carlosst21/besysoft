DROP TABLE IF EXISTS empresas;

CREATE TABLE empresas (
  id bigint auto_increment PRIMARY KEY,
  razonsocial VARCHAR(100) NOT NULL,
  direccion VARCHAR(150)
);

DROP TABLE IF EXISTS ciudades;

CREATE TABLE ciudades (
  id bigint auto_increment PRIMARY KEY,
  descripcion VARCHAR(100) NOT NULL
);

DROP TABLE IF EXISTS contactos;

CREATE TABLE contactos (
  id bigint auto_increment PRIMARY KEY,
  nombre VARCHAR(100) NOT NULL,
  direccion VARCHAR(100),
  telefono VARCHAR(15),
  mail VARCHAR(50),
  id_ciudad int,
  id_empresa int
);

ALTER TABLE contactos
    ADD FOREIGN KEY (id_ciudad) 
    REFERENCES ciudades(id)

